% Author: David Kristiansen <davidandree at gmail.com>
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{usn_thesis}[2016/04/07 thesis style]


\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions\relax
\LoadClass{book}


% === Required Packages
%\RequirePackage{fontspec} %
\RequirePackage{microtype}
\RequirePackage{setspace} % Set line spacing
\RequirePackage{parskip} % Make a space between paragraphs
\RequirePackage{geometry} % Geometry
\RequirePackage{fancyhdr}
\RequirePackage{etoolbox}
\RequirePackage{caption}
\RequirePackage{titlesec}
\RequirePackage{geometry}



% === Document style
\onehalfspacing
\geometry{
	inner=30mm,
	top=25mm,
	bottom=25mm,
	outer=25mm
}


% === Header and Footer
\newcommand*{\fancyOffset}{10mm}
\newcommand*{\fancyRuler}{.6pt}
\newcommand*{\pnumbar}{% Høyden på streken
	\raisebox{-.5pt}[\ht\strutbox][\dp\strutbox]{%
		\rule[-\dp\strutbox]{\fancyRuler}{1.1\baselineskip}%
	}%
}
\fancyhf{}
\renewcommand{\headrulewidth}{\fancyRuler}
%\renewcommand{\footrulewidth}{\fancyRuler} 
\fancyheadoffset[RO]{\fancyOffset}
\if@twoside
	\fancyheadoffset[LE]{\fancyOffset}
\fi

\fancyhead[RO]{%
	\footnotesize\sffamily\leftmark\makebox[\fancyOffset]{\makebox[-\fancyOffset][l]{\pnumbar\enskip\normalfont\normalsize\raisebox{-1.2pt}{\thepage}}}%
}
\fancyfoot[LO]{%
%	\footnotesize\sffamily{\color{gray}\today}
}

\if@twoside
	\fancyfoot[RE]{%
%		\footnotesize\sffamily{\color{gray}\today}
	}
	\fancyhead[LE]{%
		\makebox[\fancyOffset]{\makebox[-\fancyOffset][r]{\normalfont\normalsize\raisebox{-1.2pt}{\thepage}\enskip\pnumbar}}\footnotesize\sffamily\rightmark%
	}
\fi

\pagestyle{fancy}

\fancypagestyle{plain}{%
	\fancyhf{}
	\renewcommand{\headrulewidth}{0pt}
	\renewcommand{\footrulewidth}{0pt}
    \fancyhead[RO]{%
		\footnotesize\sffamily\makebox[\fancyOffset]{\makebox[\dimexpr-\fancyOffset + \fancyRuler\relax][l]{\enskip\normalfont\normalsize\raisebox{-1.2pt}{\thepage}}}%
	}

    \fancyfoot[LO]{%
%		\footnotesize\sffamily{\color{gray}\today}
	}

	\if@twoside
		\fancyhead[LE]{%
			\makebox[\fancyOffset]{\makebox[\dimexpr-\fancyOffset + \fancyRuler\relax][r]{\normalfont\normalsize\raisebox{-1.2pt}{\thepage}\enskip}}%\footnotesize\sffamily\leftmark%
		}
    	\fancyfoot[RE]{%
%			\footnotesize\sffamily{\color{gray}\today}
		}
	\fi
}
\fancypagestyle{empty}{%
	\fancyhf{}
	\renewcommand{\headrulewidth}{0pt}
	\renewcommand{\footrulewidth}{0pt}
	\fancyfoot[LO]{%
%		\footnotesize\sffamily{\color{gray}H15E01}
	}

	\if@twoside
	    \fancyfoot[RE]{%
%			\footnotesize\sffamily{\color{gray}\today}
		}
	\fi
}

\setlength{\headheight}{15pt}

\makeatletter
	\def\emptypage@emptypage{%
		\hbox{}%
		\vspace*{\fill}
		\begin{center}
			\thispagestyle{empty}	
		\end{center}
		\vspace{\fill}
		\newpage%
	}%
	\def\cleardoublepage{%
		\clearpage%
		\if@twoside%
			\ifodd\c@page%
				% do nothing
			\else%
				\emptypage@emptypage%
			\fi%
		\fi%
	}%
\makeatother



\captionsetup{
	font=sf,
	labelfont={sf,bf},
	belowskip=1em,
	aboveskip=1em
}
\renewcommand{\arraystretch}{1.2}

% Chapter
%\titleformat{\chapter}[block]
%	{\sffamily\Large\bfseries}
%	{\thechapter}
%	{1ex}
%	{}
%	[]
%\titlespacing*{\chapter}{0pt}{0pt}{0pt}

% Section
\titleformat{\section}[block]
	{\sffamily\Large\bfseries}
	{\thesection}
	{1ex}
	{}
	[]
\titlespacing*{\section}{0pt}{0pt}{0pt}

%Subsection
\titleformat{\subsection}[block]
	{\sffamily\large\bfseries}
	{\thesubsection}
	{1ex}
	{}
	[]
\titlespacing*{\subsection}{0pt}{0pt}{0pt}

% Subsubsection
\titleformat{\subsubsection}[block]
	{\sffamily\bfseries}
	{\thesubsubsection}
	{1ex}
	{}
	[]
\titlespacing*{\subsubsection}{0pt}{0pt}{0pt}

% Paragraph
\titleformat{\paragraph}[runin]
	{\itshape}
	{\theparagraph}
	{1em}
	{}
	[\hspace*{2ex}]
\titlespacing*{\paragraph}{0pt}{0pt}{0pt}

% === Less badbox warnings
\hfuzz=20pt
\vfuzz=20pt
\hbadness=2000
\vbadness=\maxdimen
% === Draw box around badboxes, requires draft mode
%\overfullrule=5pt

